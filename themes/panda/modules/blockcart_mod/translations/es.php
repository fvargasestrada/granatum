<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcart_mod}panda>blockcart-mobilebar_a85eba4c6c699122b2bb1387ea4813ad'] = 'Cesta';
$_MODULE['<{blockcart_mod}panda>blockcart-rightbar_0c3bf3014aafb90201805e45b5e62881'] = 'Ver mi carro de compra';
$_MODULE['<{blockcart_mod}panda>blockcart-rightbar_a85eba4c6c699122b2bb1387ea4813ad'] = 'Cesta';
$_MODULE['<{blockcart_mod}panda>blockcart-side_f373b403975d367491574fb2c739fef7'] = 'Cesta';
$_MODULE['<{blockcart_mod}panda>blockcart-side_d3d2e617335f08df83599665eef8a418'] = 'Cerrar';
$_MODULE['<{blockcart_mod}panda>blockcart-side_ed6e9a09a111035684bb23682561e12d'] = 'Eliminar este producto de mi carro de la compra';
$_MODULE['<{blockcart_mod}panda>blockcart-side_e7a6ca4e744870d455a57b644f696457'] = '¡Gratis!';
$_MODULE['<{blockcart_mod}panda>blockcart-side_4b7d496eedb665d0b5f589f2f874e7cb'] = 'Detalle del producto';
$_MODULE['<{blockcart_mod}panda>blockcart-side_3d9e3bae9905a12dae384918ed117a26'] = 'Personalización #%d:';
$_MODULE['<{blockcart_mod}panda>blockcart-side_09dc02ecbb078868a3a86dded030076d'] = 'Ningún producto';
$_MODULE['<{blockcart_mod}panda>blockcart-side_f2a6c498fb90ee345d997f888fce3b18'] = 'Borrar';
$_MODULE['<{blockcart_mod}panda>blockcart-side_c6995d6cc084c192bc2e742f052a5c74'] = '¡Envio gratuito!';
$_MODULE['<{blockcart_mod}panda>blockcart-side_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Envio';
$_MODULE['<{blockcart_mod}panda>blockcart-side_4b78ac8eb158840e9638a3aeb26c4a9d'] = 'Impuestos';
$_MODULE['<{blockcart_mod}panda>blockcart-side_96b0141273eabab320119c467cdcaf17'] = 'Total';
$_MODULE['<{blockcart_mod}panda>blockcart-side_0d11c2b75cf03522c8d97938490466b2'] = 'Iva incluido en el precio';
$_MODULE['<{blockcart_mod}panda>blockcart-side_41202aa6b8cf7ae885644717dab1e8b4'] = 'Iva no incluido en el precio';
$_MODULE['<{blockcart_mod}panda>blockcart-side_377e99e7404b414341a9621f7fb3f906'] = 'Comprar';
$_MODULE['<{blockcart_mod}panda>blockcart_0c3bf3014aafb90201805e45b5e62881'] = 'Ver mi carrito de la compra';
$_MODULE['<{blockcart_mod}panda>blockcart_f373b403975d367491574fb2c739fef7'] = 'Cesta';
$_MODULE['<{blockcart_mod}panda>blockcart_3812464e9c462762cb7f1b1f3a8f0e30'] = 'Productos';
$_MODULE['<{blockcart_mod}panda>blockcart_ed6e9a09a111035684bb23682561e12d'] = 'Eliminar este producto de mi cesta de compra';
$_MODULE['<{blockcart_mod}panda>blockcart_e7a6ca4e744870d455a57b644f696457'] = '¡Gratis!';
$_MODULE['<{blockcart_mod}panda>blockcart_4b7d496eedb665d0b5f589f2f874e7cb'] = 'Detalle del producto';
$_MODULE['<{blockcart_mod}panda>blockcart_3d9e3bae9905a12dae384918ed117a26'] = 'Personalización #%d:';
$_MODULE['<{blockcart_mod}panda>blockcart_09dc02ecbb078868a3a86dded030076d'] = 'Ningún producto';
$_MODULE['<{blockcart_mod}panda>blockcart_f2a6c498fb90ee345d997f888fce3b18'] = 'Borrar';
$_MODULE['<{blockcart_mod}panda>blockcart_03e9618cc6e69fe15a57c7377827a804'] = 'Determinar';
$_MODULE['<{blockcart_mod}panda>blockcart_c6995d6cc084c192bc2e742f052a5c74'] = '¡Envio Gratuito!';
$_MODULE['<{blockcart_mod}panda>blockcart_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Envio';
$_MODULE['<{blockcart_mod}panda>blockcart_4b78ac8eb158840e9638a3aeb26c4a9d'] = 'Impuestos';
$_MODULE['<{blockcart_mod}panda>blockcart_96b0141273eabab320119c467cdcaf17'] = 'Total';
$_MODULE['<{blockcart_mod}panda>blockcart_0d11c2b75cf03522c8d97938490466b2'] = 'Iva incluido en el precio';
$_MODULE['<{blockcart_mod}panda>blockcart_41202aa6b8cf7ae885644717dab1e8b4'] = 'Iva no incluido en el precio';
$_MODULE['<{blockcart_mod}panda>blockcart_377e99e7404b414341a9621f7fb3f906'] = 'Comprar';
$_MODULE['<{blockcart_mod}panda>blockcart_98b3009e61879600839e1ee486bb3282'] = 'Cerrar ventana';
$_MODULE['<{blockcart_mod}panda>blockcart_694e8d1f2ee056f98ee488bdc4982d73'] = 'Cantidad';
$_MODULE['<{blockcart_mod}panda>blockcart_544c3bd0eac526113a9c66542be1e5bc'] = 'Producto añadido correctamente a su carrito de compras';
$_MODULE['<{blockcart_mod}panda>blockcart_331d5bbe98ec28964732e957d4024d16'] = 'Hay [1]% d [/ 1] productos en su carrito.';
$_MODULE['<{blockcart_mod}panda>blockcart_fa44fb9962a66be68672e4b1d507e8b1'] = 'Hay 1 producto en su cesta.';
$_MODULE['<{blockcart_mod}panda>blockcart_db205f01b4fd580fb5daa9072d96849d'] = 'Total productos';
$_MODULE['<{blockcart_mod}panda>blockcart_21034ae6d01a83e702839a72ba8a77b0'] = '(iva excl.)';
$_MODULE['<{blockcart_mod}panda>blockcart_1f87346a16cf80c372065de3c54c86d9'] = '(iva incl.)';
$_MODULE['<{blockcart_mod}panda>blockcart_f4e8b53a114e5a17d051ab84d326cae5'] = 'Envio total';
$_MODULE['<{blockcart_mod}panda>blockcart_300225ee958b6350abc51805dab83c24'] = 'Añadir más productos';
$_MODULE['<{blockcart_mod}panda>blockcart_7e0bf6d67701868aac3116ade8fea957'] = 'Proceder al pago';
$_MODULE['<{blockcart_mod}panda>blockcart_20351b3328c35ab617549920f5cb4939'] = 'Personalización #';
$_MODULE['<{blockcart_mod}panda>crossselling_ef2b66b0b65479e08ff0cce29e19d006'] = 'Los clientes que compraron este producto también compraron';
