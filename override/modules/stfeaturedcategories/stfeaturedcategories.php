<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
if (!defined('_PS_VERSION_'))
	exit;
    


class StFeaturedCategoriesOverride extends StFeaturedCategories
{
    
           
	
    private function getPriceFrom($catId){
        $sql = "select round(ps_product.price * (1 + (ps_tax.rate / 100)), 0) as precio
        from ps_product
        join ps_tax on ps_product.id_tax_rules_group = ps_tax.id_tax
        join ps_category_product on ps_category_product.id_product = ps_product.id_product
        and ps_category_product.id_category = $catId
        order by ps_product.price ASC
        limit 1";
        
        if ($result = Db::getInstance()->ExecuteS($sql)){
            return $result[0]['precio'];
        }

        return 0;
    }
    

	private function _prepareHook($location= null)
    {
        if (!empty(self::$cache_featured_categories))
            $featured_categories = self::$cache_featured_categories;
        else
        {
            $featured_categories = StFeaturedCategoriesClass::getAll();
            self::$cache_featured_categories = $featured_categories;
        }
        
        if(!$featured_categories)
            return false;

        foreach ($featured_categories as $key => $category){
            $featured_categories[$key]['price_from'] = $this->getPriceFrom($category['id_category']);
        }
		$this->smarty->assign(array(
            'featured_categories'   => $featured_categories,
            'categorySize'          => Image::getSize(ImageType::getFormatedName('category')),
            'homeSize'            => Image::getSize(ImageType::getFormatedName('home')),
            'pro_per_xl'            => (int)Configuration::get('STSN_FEATURED_CATE_PER_XL'),
            'pro_per_lg'            => (int)Configuration::get('STSN_FEATURED_CATE_PER_LG'),
            'pro_per_md'            => (int)Configuration::get('STSN_FEATURED_CATE_PER_MD'),
            'pro_per_sm'            => (int)Configuration::get('STSN_FEATURED_CATE_PER_SM'),
            'pro_per_xs'            => (int)Configuration::get('STSN_FEATURED_CATE_PER_XS'),
            'pro_per_xxs'           => (int)Configuration::get('STSN_FEATURED_CATE_PER_XXS'),
            
            
            'slider_slideshow'      => Configuration::get('ST_PRO_CATE_F_C_SLIDESHOW'),
            'slider_s_speed'        => Configuration::get('ST_PRO_CATE_F_C_S_SPEED'),
            'slider_a_speed'        => Configuration::get('ST_PRO_CATE_F_C_A_SPEED'),
            'slider_pause_on_hover' => Configuration::get('ST_PRO_CATE_F_C_PAUSE_ON_HOVER'),
            'rewind_nav'            => Configuration::get('ST_PRO_CATE_F_C_REWIND_NAV'),
            'lazy_load'             => Configuration::get('ST_PRO_CATE_F_C_LAZY'),
            'slider_move'           => Configuration::get('ST_PRO_CATE_F_C_MOVE'),
            'hide_mob'              => Configuration::get('ST_PRO_CATE_F_C_HIDE_MOB'),
            'aw_display'            => Configuration::get('ST_PRO_CATE_F_C_AW_DISPLAY'),
            'display_as_grid'       => Configuration::get('ST_PRO_CATE_F_C_GRID'),
            'title_position'        => Configuration::get('ST_PRO_CATE_F_C_TITLE'),
            'direction_nav'         => Configuration::get('ST_PRO_CATE_F_C_DIRECTION_NAV'),
            'control_nav'           => Configuration::get('ST_PRO_CATE_F_C_CONTROL_NAV'),
        ));
        return true;
    }
    
	public function hookDisplayHome($params, $hook_hash = '')
	{
	    if (!$this->isCached('stfeaturedcategories.tpl', $this->getCacheId($hook_hash)))
    	    if(!$this->_prepareHook())
                return false;
        if (!$hook_hash)
            $hook_hash = $this->getHookHash(__FUNCTION__);
        $this->smarty->assign(array(
            'hook_hash' => $hook_hash
        ));
		return $this->display(__FILE__, 'stfeaturedcategories.tpl', $this->getCacheId($hook_hash));
	}
    
    
}
